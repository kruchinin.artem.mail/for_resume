package mainFile;

// ������� ����� ���������� � ������� ������ (������). 
// ���������� ����� �� ����

public class Result {
	private long result;
	private DateCalc dataCalc = new DateCalc();
	
	public Result(long result) {
		this.result = result;
	}
	
	public double getResult() {
		return result;
	}
	
	public DateCalc getDataCalc() {
		return dataCalc;
	}

	@Override
	public String toString() {
		return "Time = " + dataCalc + "     " +"Result = " + result;// + " Time = " + dataCalc;
	}

}
