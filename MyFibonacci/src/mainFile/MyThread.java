package mainFile;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import Write_Read_Numbers.WriteNumbersAndDate;

public class MyThread {
	// ���� ����� � ��������� ������! ����� �2 ����
	// ����������� ��� �������� ����� MAX
	// ������ �����������
	
	Map<DateCalc, Result> linkedHashMap = new LinkedHashMap<>();
	BlockingQueue<Result> myQueue = new ArrayBlockingQueue<>(10);
	Mathimathic mathimathic = new Mathimathic();	
	WriteNumbersAndDate writeNumbers = new WriteNumbersAndDate();
	
	
	public void startThread() throws InterruptedException {
	
		Thread thread1 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					arrayToArray();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}});
		
		
		Thread thread2 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					writeInFile();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}});
		
		// 1000 - ? // 10000 - 38-48     ���� ��� ���������������
		// ��� ��������, ��� ������ ����� �������� ��������� ������ �� ����������
		// (1/n) 1000 - 5s // 10000 - 29s // 100_000 - ��� (1)	-
		// (1%n) 1000 - 5s // 10000 - 30s // 100_000 - ?		- ������������ ��������
		// (1)   1000 - 7s // 10000 - 29s // 100_000 - 262s		- �� ������� �������� ��������� ��������
		// (n)   1000 - 7s // 10000 - 39s // 100_000 - 352s 	- ����� �������� �������� ��� ��������
		// (n/10)1000 - 5s // 10000 - 31s // 100_000 - 264s 	- �������� �������� ��� ��������, 1 ������� �� ����� �� n = 30
		// (time1-time2) 1000 - 10s // 10000 - 60s  			- ������, ����� ������ � ���� �������, ��� ���������� � �������
		
		thread1.start();
		
		thread2.start();
		thread1.join();
		if(mathimathic.getMax()>100) {
			Thread.sleep(mathimathic.getMax()/10);
		} else {
			Thread.sleep(5);
		}
		thread2.interrupt();
		thread2.join();
	}
		
	private void arrayToArray() throws InterruptedException {
			mathimathic.mathimathic(linkedHashMap);
			for(Entry<DateCalc, Result> entry : linkedHashMap.entrySet()) {	
				try {
					myQueue.put(entry.getValue());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}	
			}
	}
	
	private void writeInFile() throws InterruptedException {
		while(true) {
			try {
				if(!myQueue.isEmpty()) {
				writeNumbers.writeNumbersAndDate(myQueue.take());
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(Thread.currentThread().isInterrupted())
				break;
		}
	}
}
