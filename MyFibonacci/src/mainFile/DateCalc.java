package mainFile;

import java.util.Calendar;
import java.util.Objects;

// ������� ����� ������������ ���� ���������� 
// �� ���� HH:MM DD:MM:YYYY
// �������������� ������ ��� ���������� ����������
public class DateCalc implements Comparable<DateCalc>{
	long timer = System.currentTimeMillis();
	Calendar calendar = Calendar.getInstance();
	
	private int minute = calendar.get(Calendar.MINUTE);
	private int hour = calendar.get(Calendar.HOUR_OF_DAY);
	private int day = calendar.get(Calendar.DATE);
	private int month = calendar.get(Calendar.MONTH);
	private int year = calendar.get(Calendar.YEAR);
	
	public int getHour() {
		return hour;
	}
	public int getMinute() {
		return minute;
	}
	public int getDay() {
		return day;
	}
	public int getMonth() {
		return month;
	}
	public int getYear() {
		return year;
	}
	
	public String toString() {
		return hour + ":" + minute + "   " 
				+ day + ":" + (month+1) + ":" + year;
	}
	@Override
	// ����� ��������� �������� �� ���������� timer???
	public int hashCode() {
		return Objects.hash(calendar, day, hour, minute, month, timer, year);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DateCalc other = (DateCalc) obj;
		return Objects.equals(calendar, other.calendar) && day == other.day && hour == other.hour
				&& minute == other.minute && month == other.month && timer == other.timer && year == other.year;
	}
	@Override
	public int compareTo(DateCalc o) {
		if(this.hashCode() > o.hashCode()) {
		return 1;
		} else if (this.hashCode() < o.hashCode()) {
			return -1;
		} else {
			return 0;
		}
		
	}
	
}
