package scaner_import;

import java.util.Scanner;

public abstract class ScannerImport {
	
Scanner scan = new Scanner(System.in);
	
	protected int scanInt() {
		int n = scan.nextInt();
		return n;
	}
	
	protected String scanString() {
		String n = scan.next();
		return n;
	}
	}
