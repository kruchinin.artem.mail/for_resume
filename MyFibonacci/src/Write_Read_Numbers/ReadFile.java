package Write_Read_Numbers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadFile {
	Scanner scan;
	
	public void readFile () {
		File testFile123 = new File("MyDataBase.txt");
		try {
			 scan = new Scanner(testFile123);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		while(scan.hasNext()) {
			String line = scan.nextLine();
			System.out.println(line);
		} 
		scan.close();
	}
}
