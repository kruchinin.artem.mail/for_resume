package Write_Read_Numbers;

import java.io.FileWriter;
import java.io.PrintWriter;

import mainFile.Result;

public class WriteNumbersAndDate {

	public void writeNumbersAndDate (Result result) {
		try (PrintWriter pw = new PrintWriter(new FileWriter("MyDataBase.txt", true))) {
			pw.println(result);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

