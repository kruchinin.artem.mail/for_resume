import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet("/ShowAllResults")
public class ShowAllResults extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        try (PrintWriter pw = resp.getWriter()) {
                try {
                    ResultSet resultSet = SingleConn.getInstance().statement.executeQuery("SELECT number FROM fibonacci");

                    while (resultSet.next()) {
                        pw.println(resultSet.getString("number") + "<br>");
                    }
                } catch (SQLException e) {
                    pw.println(e);
                    throw new RuntimeException(e);
                }

            pw.println("<p> <a href=\"SendNumber.jsp\"> Ввести новое число </a> </p>");
            pw.println("<p> <a href=\"index.jsp\"> На главную </a> </p>");
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       doGet(req, resp);
    }
}
