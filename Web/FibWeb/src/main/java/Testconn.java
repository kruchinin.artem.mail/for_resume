import java.sql.ResultSet;
import java.sql.SQLException;

public class Testconn {
    public static void main(String[] args) {

        try {
            ResultSet resultSet = SingleConn.getInstance().statement.executeQuery("SELECT number FROM websql ORDER BY id DESC LIMIT 1");
            while (resultSet.next()) {
                System.out.println(resultSet.getString("number"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
