import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/AddFibonacci")
public class AddFibonacci extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        int m1 = 0;
        m1 = Integer.parseInt(req.getParameter("m1").trim());
        System.out.println("Enter "+ m1);

        new ClearDB().clearDB();

        try {
            m1 = Integer.parseInt(req.getParameter("m1").trim());
            System.out.println("Enter "+ m1);
            try (PrintWriter pw = resp.getWriter()){

                MathimathicFib mathimathicFib = new MathimathicFib( m1 );
                List<BigInteger> list = mathimathicFib.getArrayListFib();
                for(BigInteger list1 : list) {
                    try {
                        SingleConn.getInstance().statement.executeUpdate("INSERT INTO fibonacci(number)" + " VALUES (" + list1.toString() + ")");
                    } catch (SQLException e) {
                        pw.println(e);
                        throw new RuntimeException(e);
                    }
                }
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/ChoseResult");
                requestDispatcher.forward(req,resp);
            }
        }catch (Exception e){
            System.out.println("Enter char!");
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/SendNumber");
            requestDispatcher.forward(req,resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
