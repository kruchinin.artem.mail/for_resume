import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class MathimathicFib {
    private int max;
    private List<BigInteger> list = new ArrayList<>(max);

    public MathimathicFib(int max){
        this.max=max;
    }

    public List<BigInteger> getArrayListFib(){

        for(int i = 0; i < max; i++){
            list.add(BigInteger.valueOf(i));
        }
        for(int i = 0; i < list.size(); i++){
            list.set(i, mathFib(list, i));
        }

        return list;
    }

    private BigInteger mathFib(List<BigInteger> array, int n) {
        if(n <= 1) {
            return BigInteger.valueOf(n);
        } else {
            return (array.get(n-1)).add(array.get(n-2));//array.get(n-1)+array.get(n-2);
        }
    }

}
