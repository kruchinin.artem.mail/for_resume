import java.sql.ResultSet;
import java.sql.SQLException;

public class ClearDB {
    public void clearDB() {
        try {
            SingleConn.getInstance().statement.executeUpdate("DELETE FROM fibonacci");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
