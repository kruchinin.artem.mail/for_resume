
import java.sql.*;

import static jdk.javadoc.internal.doclets.formats.html.markup.HtmlStyle.title;

public class SingleConn {
    Statement statement;
    private static volatile SingleConn conn = null;
    private static final Object sync = new Object();

    private SingleConn(){}

    public static SingleConn getInstance(){
        if(conn == null){
            synchronized (sync){
                if(conn == null) {
                    conn = new SingleConn();
                    conn.getConn();
                }
            }
        }
        return conn;
    }

    private Statement getConn() {
       try {
           Class.forName("org.postgresql.Driver");
       } catch (ClassNotFoundException e) {
           throw new RuntimeException(e);
       }

       try {
           Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/FibWeb", "postgres", "admin");
           statement  = conn.createStatement();
           System.out.println("Connected!");

       } catch (SQLException e) {
           throw new RuntimeException(e);
       }
       return statement;
    }

}


